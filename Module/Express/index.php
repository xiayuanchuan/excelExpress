<?php

/**
 * 查询物流的方法  一步获取物流信息  也可以将格式化好的数据给他
 * 
 */
require_once LIBRARY_PATH . "Express.class.php";

/**
 * 异步方式获取   返回json
 */
function loadExpressAjax() {
    $expressObj = new Express();
    $name = $_GET['invoice_name'];
    $order = $_GET['invoice_nu'];
    $json = $expressObj->getorder($name, $order);
    GlobalFunction::returnJSON(true, $json);
}

/**
 * 异步方式获取   返回html
 */
function loadExpressHtml() {
    $expressObj = new Express();
    $name = $_GET['invoice_name'];
    $order = $_GET['invoice_nu'];
    $json = $expressObj->getorder($name, $order);
    $html = "";
//    <table id="queryResult" class="result-info" cellspacing="0"><tbody><tr class="last"><td class="row1"><span class="day">2018.10.14</span><span class="time">17:21</span>&nbsp;&nbsp;<span class="week">星期天</span></td><td class="status status-wait">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[新疆乌鲁木齐分拨中心]在分拨中心进行卸车扫描</td></tr><tr><td class="row1"><span class="day">2018.10.12</span><span class="time">04:45</span></td><td class="status">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[江苏苏州分拨中心]进行装车扫描，发往：新疆乌鲁木齐分拨中心</td></tr><tr><td class="row1"><span class="day">2018.10.12</span><span class="time">04:41</span></td><td class="status">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[江苏苏州分拨中心]在分拨中心进行卸车扫描</td></tr><tr><td class="row1"><span class="day">2018.10.12</span><span class="time">01:26</span></td><td class="status">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[上海分拨中心]进行装车扫描，发往：江苏苏州分拨中心</td></tr><tr><td class="row1"><span class="day">2018.10.12</span><span class="time">00:44</span>&nbsp;&nbsp;<span class="week">星期五</span></td><td class="status">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[上海分拨中心]进行中转集包扫描，发往：新疆乌鲁木齐网点包</td></tr><tr><td class="row1"><span class="day">2018.10.11</span><span class="time">21:29</span></td><td class="status">&nbsp;<div class="col2"><span class="step"><span class="line1"></span><span class="line2"></span></span></div></td><td class="context">[上海分拨中心]在分拨中心进行称重扫描</td></tr><tr><td class="row1"><span class="day">2018.10.11</span><span class="time">16:38</span>&nbsp;&nbsp;<span class="week">星期四</span></td><td class="status status-first">&nbsp;</td><td class="context">[<a href="https://www.kuaidi100.com/network/?from=fonter1&amp;searchText=%E4%B8%8A%E6%B5%B7%E9%95%BF%E5%AE%81%E5%8C%BA%E4%B8%B4%E7%A9%BA%E5%85%AC%E5%8F%B8%E5%B8%82%E5%9C%BA%E9%83%A8%E9%A1%B9%E7%9B%AE%E9%83%A8&amp;company=2" target="_blank">上海长宁区临空公司市场部项目部</a>]进行揽件扫描</td></tr></tbody></table>
}
