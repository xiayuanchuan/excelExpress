<?php

/**
 * 
 * 处理上传文件 
 */
function upload() {
    //判断是否上传文件  没上传 提提示错误信息
    $gb = GlobalFunction::getInstance();
    if (!isset($_FILES['userfile'])) {
        //  文件未上传，则提示需要上传文件
        GlobalFunction::showErrorMsg("请上传文件后再提交", "/index.php?m=Admin&c=index");
    }
    $file_types = explode(".", $_FILES ['userfile'] ['name']);
    $ext = array_pop($file_types);

    /* 判别是不是.xls文件，判别是不是excel文件 */
    if (strtolower($ext) != "xlsx" && strtolower($ext) != "xls") {
        GlobalFunction::showErrorMsg("请上传excel文件", "/index.php?m=Admin&c=index");
    }

    $savePath = UPLOAD_PATH;
    $saveName = time() . $gb::getRandom() . ".{$ext}";
    if (!$gb->uploadFile($savePath, $saveName, $_FILES ['userfile'], $msg)) {
        GlobalFunction::showErrorMsg($msg);
    }

    //读取文件  

    $fieldMap = array(
        'A' => "user_name",
        'B' => "mobile_phone",
        'C' => "invoice_name",
        'D' => "invoice_nu",
        'E' => "order_nu",
        'F' => "goods_name",
        'G' => "goods_nu",
        'H' => "address",
        'I' => "remark"
    );
    require_once LIBRARY_PATH . "PHPExcel.php";


    $objPHPExcel = PHPExcel_IOFactory::load($savePath . $saveName);


//获取sheet0，就是excel第一个sheet
    $sheet = $objPHPExcel->getSheet(0);
    $highestRow = $sheet->getHighestRow(); // 取得总行数
//    $highestColumn = $sheet->getHighestColumn(); // 取得总列数
    //调用ThinkPHP中的M方法来实例化一个USERS模型，从而调用SQL操作
    if ($highestRow < 2) {
        GlobalFunction::showErrorMsg("请上传带有数据的excel", "/index.php?m=Admin&c=index");
    }
    $data = null;
//这个循环是一行一行的把excel里面的值取出来，存到$data数组里面
    for ($j = 2; $j <= $highestRow; $j++) {
        $data = array();
        foreach ($fieldMap as $key => $value) {
            $data[$value] = $objPHPExcel->getActiveSheet()->getCell($key . $j)->getValue();
        }

        //检测订单号 手机号 物流号是否存在

        if (!$data['order_nu'] && !$data['invoice_nu'] && !$data['mobile_phone']) {
            continue;
        }
        $data['creat_time'] = date("Y-m-d H:i:s");
        $where = " order_nu='{$data['order_nu']}'  and state >=0  and  invoice_nu='{$data['invoice_nu']}'";
        $records = $GLOBALS['db']->where($where)->limit(1)->select(OrderInfo::TABLE_NAME);
        if ($records) {
            $where = " id='{$records[0]['id']}' ";
            $GLOBALS['db']->where($where)->update(OrderInfo::TABLE_NAME, $data);
        } else {
            //新增
            $GLOBALS['db']->insert(OrderInfo::TABLE_NAME, $data);
        }
    }
    @unlink($savePath . $saveName);
    GlobalFunction::showErrorMsg("上传成功", "/index.php?m=Admin&c=info");
}

/**
 * 下载excel数据文件
 */
function download() {

    $fields = " user_name  as '姓名' , mobile_phone as '手机号' , invoice_name as '快递公司' , invoice_nu as  '快递编号' , order_nu as '订单编号' , goods_name as '商品名称' , goods_nu as '数量' , address as '地址' , remark as '备注' ";
    $where = "state>=0";
    $order = array('id' => 'asc');
    $list = $GLOBALS['db']->field($fields)
            ->order($order)
            ->where($where)
            ->select(OrderInfo::TABLE_NAME);
    // 返回数组中的第一个元素
    $expCellName = array_keys($list[0]);
    require_once LIBRARY_PATH . 'PHPExcel.php';

    $cellStyle = array(
        '快递编号' => PHPExcel_Cell_DataType::TYPE_STRING,
        '手机号' => PHPExcel_Cell_DataType::TYPE_STRING,
        '订单编号' => PHPExcel_Cell_DataType::TYPE_STRING,
    );
//  导出名称为订单加上日期的excel文件
    GlobalFunction::exportExcel("订单", $expCellName, $list, $cellStyle);
}

/**
 * 删除订单  逻辑删除 
 */
function orderdel() {
    $id = intval($_GET['id']);


    if (empty($id)) {
        GlobalFunction::returnJSON(false, [], 0, "参数不能为空");
    }

    $where = " id = '{$id}' and  state>=0";

    $result = $GLOBALS['db']->where($where)->update(OrderInfo::TABLE_NAME, array('state' => "-1"));
    if ($result) {
        GlobalFunction::returnJSON(true, []);
    } else {
        GlobalFunction::returnJSON(false, [], 0, "数据库保存失败");
    }
}
