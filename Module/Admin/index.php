<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <link rel="stylesheet" href="/Static/css/base.css" />
        <title>资料上传</title>
        <style>
            body {
                padding: 0px;
                margin: 0px;
            }
            .con {
                padding-top: 100px;
                width: 600px;
                margin: 0px 300px;
            }

            .header {
                height: 50px;
                width: auto;
                border: 1px solid black;
                line-height: 50px;
                text-align: center;
            }
            .header .header-link{
                padding-left: 15px;
            }
            .entering {
                height: 48px;
                margin-top: 27px;
                text-align: center;
            }

            .btna {
                padding: 5px 30px;
                font-size: 20px;
                border-radius: 5px;
                text-align: center;
                margin-top: 30px;
            }

            .btna.btna-b {
                background-color: white;
                border: 1px solid black;
            }

            .btna.btna-c {
                background-color:#AAAAAA;

            }
        </style>
    </head>

    <body>
        <div class="con">
            <div class="header">
                <span>请上传订单Excel</span><a class="header-link" target="view" href="/Static/导入订单数据模板.xls">(点击下载模版)</a>
            </div>
            <form action="/index.php?c=upload&m=Admin&action=upload" method="post" enctype="multipart/form-data">
                <div class="entering">
                    <input type="file" name="userfile" />
                </div>
                <div class="btna">
                    <input class="btna btna-b" type="submit" value="提交" />
                    <a class="btna btna-c" href="/index.php?c=info&m=Admin"/>查看所有</a>
                </div>
            </form>
        </div>
    </body>

</html>