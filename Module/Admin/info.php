<?php
$query = (isset($_GET['query']) && $_GET['query']) ? $_GET['query'] : '';
//如果$_GET['query']) && $_GET['query'] 为真   则query=$_GET['query']，否则为空
$page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : 1;
$pageSize = 50;
//每页30条数据
if ($query) {
    $where = " state>=0 and (user_name like '%{$query}%' or order_nu like '%{$query}%' or mobile_phone like '%{$query}%' or  invoice_nu like '%{$query}%')";
    // 前台传入的数据是否与名字，订单编号，手机号，快递编号相似  则执行此条件
} else {
    $where = "  state>=0";
    //  未传入值时，执行状态大于等于0的条件
}
$totalCount = $GLOBALS['db']->where($where)->count(OrderInfo::TABLE_NAME, "id");
//同过‘id’查询数据库中总个多少条数据   总共多少条数据
$totalPage = ceil($totalCount / $pageSize);
//  向上取整，有小数就会进一，计算会有多少页数据，如果3.5页  则返回4页   总页数

$option["url"] = "/index.php?m=Admin&c=info";
$option['currentPage'] = $page;
$option['totalPage'] = $totalPage;
$option['totalCount'] = $totalCount;
$pageObject = loadPages($option, array("query" => $query));
//dump($pageObject);
//echo $totalPage;
//echo $GLOBALS['db']->getLastSql();die;
$results = $GLOBALS['db']->where($where)->limit($page, $pageSize)->order(array('id' => "desc"))->select(OrderInfo::TABLE_NAME);

//  order(array('id' => "desc")： 以id来进行排序。
//echo $GLOBALS['db']->getLastSql();

function loadPages($option, $params) {


//处理上一页
    if ($option['currentPage'] <= 1) {
        //   当当前页小于等于1时
        $prePageUrl = "javascript:void(0);";
        //  javascript:void(0):空方法   条件为真时  不进行任何操作，让超链接执行一个js函数，而不是跳转到一个地址。
    } else {
//        $prePageUrl = $option['url'].($option['currentPage']-1)
        $params['page'] = ($option['currentPage'] - 1);
        //  当前页减1
        $prePageUrl = $option['url'] . "&" . http_build_query($params);
        // 组装URL    http_build_query($params)：将传入的数组生成URL的请求字符串
    }

//处理下一页 
    if ($option['currentPage'] >= $option['totalPage']) {
        //  当 当前页面 大于等于总页数
        $nextPageUrl = "javascript:void(0);";
        //  条件为真时，执行空操作，不进行任何操作
    } else {
        $params['page'] = ($option['currentPage'] + 1);
        $nextPageUrl = $option['url'] . "&" . http_build_query($params);
        //  条件为假时，但前页加1，并组装对应的URL
    }

    if ($option['totalPage'] == 1) {
        $prePageUrl = "javascript:void(0);";
        $nextPageUrl = "javascript:void(0);";
    }

    return array(
        "prePageUrl" => $prePageUrl,
        "nextPageUrl" => $nextPageUrl
    );
}

//$totalPage= ceil($totalCount/$pageSize);
//  得到总页数   
?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <title>数据详情</title>
        <link rel="stylesheet" href="/Static/css/base.css" />
        <style>
            body {
                padding: 0px;
                margin: 0px;
            }

            .con {
                width: 1700px;
                margin: 0px 40px;
            }

            .poll {
                text-align: center;
                margin-top: 15px;
                color: blue;
            }

            .btna {
                color: black;
            }

            .btn.btn-b {
                width: 50px;
            }

            .bod {
                text-align: center;
                margin-top: 20px;
                margin-left: auto;
                margin-right: auto;
            }

            .orderlist {
                border-collapse: collapse;
            }

            .orderlist th {
                text-align: center;
                font-weight: 700;
            }

            .orderlist th,
            .orderlist td {
                padding: 15px;
            }

            .btt {
                font-size: 15px;
                border-radius: 5px;
                margin-top: 40px;
                text-align: center;
            }

            .btt.btt-b {
                background-color: white;
                border: 1px solid black;
                width: 80px;
                height: 35px;
            }
            .orderlist tr:nth-child(2n){
                background: #f0f0f0;

            }
        </style>
        <script src="/Static/js/jquery-1.9.1.min.js"></script>
    </head>

    <body>
        <div class="con">
            <div class="poll">
                <span>查看库中所有数据</span>
            </div>
            <div class="btna">
                <form  action="/index.php" method="get">
                    <input type="hidden" name ="m" value ="Admin" />
                    <input type="hidden" name ="c" value ="info" />
                    <input type="text" name ="query" placeholder="请输入关键字查询" />
                    <input type="submit" class="btna btn-b" style="width: 100px;" value="搜索" /> <a href="/index.php?m=Admin&c=index" class="btn">上传</a>
                    <a class="btn" href="/index.php?m=Admin&c=upload&action=download">导出</a>
                </form>



            </div>
            <div class="bod">
                <table class="orderlist" border="1" cellspacing="0">
                    <tr>
                        <th>序号</th>
                        <th>姓名</th>
                        <th>手机号</th>
                        <th>快递公司</th>
                        <th>快递编号</th>
                        <th>订单编号</th>
                        <th>商品名称</th>
                        <th>数量</th>
                        <th>地址</th>
                        <th>备注</th>
                        <th>导入时间</th>
                        <th>操作</th>
                    </tr>

                    <?php
                    if (count($results)) {
                        $str = "";

                        foreach ($results as $key => $value) {
                            $index = ($key + 1);
                            $str .= <<<EOF
                    <tr>
                        <td>{$index}</td>
                        <td>{$value['user_name']}</td>                        
                        <td>{$value['mobile_phone']}</td>
                        <td>{$value['invoice_name']}</td>
                        <td>{$value['invoice_nu']}</td>
                        <td>{$value['order_nu']}</td>
                        <td>{$value['goods_name']}</td>
                        <td>{$value['goods_nu']}</td>
                        <td>{$value['address']}</td>
                        <td>{$value['remark']}</td>
                        <td>{$value['creat_time']}</td>
                        <td><a href="javascript:del({$value['id']})">删除</a> <a target='indexinfo' href="/index.php?m=Index&c=index&id={$value['id']}">查看</a></td>
                    </tr>
EOF;
                        }
                    } else {
                        $str = <<<EOF
                    <tr>
                        <td>暂无数据</td>
                    </tr>
EOF;
                    }
                    echo $str;
                    ?>
                </table>

            </div>
            <div class="btt">
                <a  class="btn"  href="<?php echo $pageObject['prePageUrl'];
                    ?>" >上一页</a>
                <span><span><?php echo $page; ?></span>/<span><?php echo $totalPage; ?></span>页</span>
                <a  class="btn"href="<?php echo $pageObject['nextPageUrl']; ?>" >下一页</a>
            </div>
        </div>
    </body>
    <script>
        function del(id) {
            if (confirm("您确认要删除吗？")) {
                $.getJSON("/index.php?m=Admin&c=upload&action=orderdel", {id: id}, function (ret) {
                    if (ret.success) {
                        alert("删除成功");
                        window.location.href = "";
                    } else {
                        alert(ret.msg);
                    }

                });
            }

        }
    </script>

</html>