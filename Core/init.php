<?php

header("Content-Type: text/html;charset=utf-8");
error_reporting(E_ALL ^ E_NOTICE);

// 定义框架目录常量
if (!defined("CORE_PATH")) {
    define("CORE_PATH", dirname(__FILE__) . DIRECTORY_SEPARATOR);
}

require_once CORE_PATH . 'constant.php';

loadConstantFiles();

require_once CONFIG_PATH . 'config.php';
require_once CONFIG_PATH . 'db.php';
require_once LIBRARY_PATH . 'Mysql.class.php';
require_once FUNCTION_PATH . 'GlobalFunction.php';
//将数据库的操作对象放到GLOBALE中
//模块路由
$module = (isset($_GET['m']) && $_GET['m']) ? $_GET['m'] : "Index";
$moduleFile = (isset($_GET['c']) && $_GET['c']) ? $_GET['c'] : "index";
//拼接路径
$dealFile = MODULE_PATH . $module . DS . $moduleFile . ".php";
//echo $dealFile;die;
if (!file_exists($dealFile)) {
    GlobalFunction::showErrorMsg("访问的模块有误");
} else {
    
    loadPOFiles();
//直接使用$GLOBALS['db'] 操作数据库即可
    $db = $GLOBALS['db'] = new Mysql($GLOBALS['config']['db']);
    require_once $dealFile;
}
//有时候需要通过action来 切换方法 在同一个文件内  
$action = (isset($_GET['action']) && $_GET['action']) ? $_GET['action'] : null;
//如果设置了方法 就可以调用放发
if ($action) {
    if (function_exists($action)) {
        //存在  就调用方法  然后结束脚本
        call_user_func($action);
        die;
    } else {
        GlobalFunction::showErrorMsg("访问的方法不存在");
    }
}

/**
 * 加载 CONSTANT_PATH 目录下的所有文件
 */
function loadConstantFiles() {
    //FIXME  需要遍历文件夹 加载文件 
//    require_once CONSTANT_PATH . "OrderInfo.class.php";
}

function loadPOFiles() {
    //FIXME  需要遍历文件夹 加载文件 
    require_once PO_PATH . "OrderInfo.php";
}

?>