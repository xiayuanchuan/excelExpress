<?php

// 目录分隔符
if (!defined("DS")) {
    define("DS", DIRECTORY_SEPARATOR);
}
if (!defined("CORE_PATH")) {
    define("CORE_PATH", dirname(__FILE__) . DS);
}
//网站根目录
if (!defined("WEB_ROOT")) {
    define("WEB_ROOT", dirname(CORE_PATH) . DS);
}
// 配置文件
if (!defined("CONFIG_PATH")) {
    define("CONFIG_PATH", CORE_PATH . "Config" . DS);
}
//共用方法
if (!defined("FUNCTION_PATH")) {
    define("FUNCTION_PATH", CORE_PATH . "Function" . DS);
}

//扩展库
if (!defined("LIBRARY_PATH")) {
    define("LIBRARY_PATH", CORE_PATH . "Library" . DS);
}
//应用木模块
if (!defined("MODULE_PATH")) {
    define("MODULE_PATH", WEB_ROOT . "Module" . DS);
}
if (!defined("PO_PATH")) {
    define("PO_PATH", MODULE_PATH . "PO" . DS);
}
//静态资源文件夹
if (!defined("STATIC_PATH")) {
    define("STATIC_PATH", WEB_ROOT . "Static" . DS);
}

//静态资源文件夹
if (!defined("CONSTANT_PATH")) {
    define("CONSTANT_PATH", CORE_PATH . "Constant" . DS);
}

//应用级别的常量
//定义上传文件的根目录
if (!defined("UPLOAD_PATH")) {
    define("UPLOAD_PATH", WEB_ROOT . "Upload" . DS);
}
?>